#include <cmath>
#include "../src/LeastSquares.h"
#include "../src/Data.h"
#include <Eigen>
#include <fstream>
#include <gtest/gtest.h>

// The fixture for testing class Foo.
class LeastSquaresTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if their bodies would
  // be empty.

  LeastSquaresTest() {
     // You can do set-up work for each test here.
     nb_basis_elements = 6;
     X_eval = Eigen::VectorXd {{1.0, 2.4, 3.2, -4.5}};
     input_file_name = "../input_files/input_test_ls.txt";
     nb_rows = 101;

  }

  ~LeastSquaresTest() override {
     // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  void SetUp() override {
     // Code here will be called immediately after the constructor (right
     // before each test).

  }

  void TearDown() override {
     // Code here will be called immediately after each test (right
     // before the destructor).
  }

  // Class members declared here can be used by all tests in the test suite
  // for Foo.
  std::string input_file_name;
  int nb_rows;
  int nb_basis_elements;
  Eigen::VectorXd X_eval;



};

// test the design matrix

// test the result of the fitting

TEST_F(LeastSquaresTest, fit) {
    std::ifstream input_file(input_file_name);
    assert(input_file.is_open());
    Data data(input_file, nb_rows);
    input_file.close();
    PolynomialLeastSquares poly_ls(data, nb_basis_elements);
    poly_ls.fit();
    Eigen::VectorXd exp_beta{{1.649, 1.470, -2.818, 0.492, -0.007, -0.199}};
    ASSERT_TRUE(exp_beta.isApprox(poly_ls.get_beta(), 0.001));
}

TEST_F(LeastSquaresTest, evaluate) {
    std::ifstream input_file(input_file_name);
    assert(input_file.is_open());
    Data data(input_file, nb_rows);
    input_file.close();
    PolynomialLeastSquares poly_ls(data, nb_basis_elements);
    poly_ls.fit();
    Eigen::VectorXd exp_eval{{0.586,  -20.315,  -73.793,  256.516}};
    ASSERT_TRUE(exp_eval.isApprox((poly_ls.evaluate(X_eval)).getY(), 0.001));
}