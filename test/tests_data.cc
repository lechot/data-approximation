#include <cmath>
#include "../src/Data.h"
#include <Eigen>
#include <fstream>
#include <gtest/gtest.h>

// The fixture for testing class Foo.
class DataTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if their bodies would
  // be empty.

  DataTest() {
     // You can do set-up work for each test here.
     input_file_name = "../input_files/input_test_data.txt";
     nb_rows = 3;

  }

  ~DataTest() override {
     // You can do clean-up work that doesn't throw exceptions here.
  }

  // If the constructor and destructor are not enough for setting up
  // and cleaning up each test, you can define the following methods:

  void SetUp() override {
     // Code here will be called immediately after the constructor (right
     // before each test).

  }

  void TearDown() override {
     // Code here will be called immediately after each test (right
     // before the destructor).
  }

  // Class members declared here can be used by all tests in the test suite
  // for Foo.
  std::string input_file_name;
  int nb_rows;

};

TEST_F(DataTest, read_file_constructor) {
    std::ifstream input_file(input_file_name);
    assert(input_file.is_open());
    Data data(input_file, nb_rows);
    input_file.close();
    ASSERT_TRUE((data.getX()).isApprox(Eigen::VectorXd {{1.0, 2.0, 3.0}}));
    ASSERT_TRUE((data.getY()).isApprox(Eigen::VectorXd {{2.5, 1.5, 4.3}}));
}

TEST_F(DataTest, vector_constructor) {
    Eigen::VectorXd X{{1.0, 2.0, 3.0}};
    Eigen::VectorXd Y{{2.5, 1.5, 4.3}};
    Data data(X, Y);
    ASSERT_TRUE((data.getX()).isApprox(Eigen::VectorXd {{1.0, 2.0, 3.0}}));
    ASSERT_TRUE((data.getY()).isApprox(Eigen::VectorXd {{2.5, 1.5, 4.3}}));
}

TEST_F(DataTest, assign_operator) {
    Eigen::VectorXd X{{1.0, 2.0, 3.0}};
    Eigen::VectorXd Y{{2.5, 1.5, 4.3}};
    Data data(X, Y);
    Data data_bis;
    data_bis = data;
    ASSERT_TRUE((data.getX()).isApprox(data_bis.getX()));
    ASSERT_TRUE((data.getY()).isApprox(data_bis.getY()));
}