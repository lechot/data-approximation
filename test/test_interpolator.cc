////
//// Created by hhammoud@INTRANET.EPFL.CH on 12.12.22.
////
#include <cmath>
#include "../src/Interpolator.h"
#include "../src/Data.h"
#include <Eigen>
#include <fstream>
#include <gtest/gtest.h>

// The fixture for testing class Foo.
class InterpolatorTest : public ::testing::Test {
protected:

    InterpolatorTest() {
        X_eval1 = Eigen::VectorXd {{-1.0, 1.5, 3.2, -4.8, 17.0}};
        X_eval2 = Eigen::VectorXd {{0.0, M_PI/6, M_PI/2, M_PI, (3*M_PI)/2}};
        input_file_name1 = "../input_files/input_test_lag.txt";
        input_file_name2 = "../input_files/input_test_fourier.txt";
        nb_rows1 = 41;
        nb_rows2 = 20;
    }

//    ~InterpolatorTest() override {}
//    void SetUp() override {}
//    void TearDown() override {}

    std::string input_file_name1;
    std::string input_file_name2;
    int nb_rows1;
    int nb_rows2;
    Eigen::VectorXd X_eval1;
    Eigen::VectorXd X_eval2;
};

TEST_F(InterpolatorTest, Standard_Lagrange) {
    std::ifstream input_file(input_file_name1);
    assert(input_file.is_open());
    Data data(input_file, nb_rows1);
    input_file.close();
    Lagrange L_standard(data);
    Eigen::VectorXd exp_outcome{{1.0, 2.25, 10.24, 23.04, 289}};
    ASSERT_TRUE(exp_outcome.isApprox(L_standard.Standard(X_eval1).getY(), 0.001));
}

TEST_F(InterpolatorTest, Bary_Centric) {
    std::ifstream input_file(input_file_name1);
    assert(input_file.is_open());
    Data data(input_file, nb_rows1);
    input_file.close();
    Lagrange L_barycentric(data);
    Eigen::VectorXd exp_outcome{{1.0, 2.25, 10.24, 23.04, 289}};
    ASSERT_TRUE(exp_outcome.isApprox(L_barycentric.BaryCentric(X_eval1).getY(), 0.001));
}

TEST_F(InterpolatorTest, Fourier) {
    std::ifstream input_file(input_file_name2);
    assert(input_file.is_open());
    Data data(input_file, nb_rows2);
    input_file.close();
    Periodic P(data);
    Eigen::VectorXd exp_outcome{{0.0, 0.5, 1.0, 0.0, -1.0}};
    ASSERT_TRUE(exp_outcome.isApprox(P.Fourier(X_eval2).getY(), 0.001));
}

