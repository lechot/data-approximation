//
// Created by lechot@INTRANET.EPFL.CH on 02.12.22.
//

#include <iostream>
#include "LeastSquares.h"
#include <cmath>

// constructor of the parent class
LeastSquares::LeastSquares(const Data& data, int nb_basis_elements)
: nb_basis_elements(nb_basis_elements)
{
    X = data.getX();
    Y = data.getY();
    nb_samples = X.size();
}

// builds the design matrix
void LeastSquares::build_design_matrix() {
    // matrix of size nb_samples x nb_basis_elements
    design_matrix = Eigen::MatrixXd::Zero(nb_samples, nb_basis_elements);
    for (int j(0); j < nb_basis_elements; ++j){
        design_matrix.block(0,j,nb_samples,1) = basis_function(j,X);
    }
}

// performs the fit using the colPivHouseholderQr method
void LeastSquares::fit() {
    beta = design_matrix.colPivHouseholderQr().solve(Y);
}

// evaluates the fitted function at the user-specified points
Data LeastSquares::evaluate(const Eigen::VectorXd& X_eval) const {
    Eigen::VectorXd Y_eval(Eigen::VectorXd::Zero(X_eval.size()));
    for(int j(0); j < beta.size(); ++j){
        Y_eval += beta[j]* basis_function(j, X_eval);
    }
    Data data(X_eval, Y_eval);
    return data;
}

Data LeastSquares::run(const Eigen::VectorXd& X_eval) {
    fit();
    return evaluate(X_eval);
}

// constructor of daughter class, that directly builds the design matrix
PolynomialLeastSquares::PolynomialLeastSquares(const Data& data, int nb_basis_elements)
: LeastSquares(data, nb_basis_elements)
{
    build_design_matrix();
}

// overridden method that defines the basis elements of the daughter class
Eigen::VectorXd PolynomialLeastSquares::basis_function(int i, const Eigen::VectorXd& X) const {
    return ((X.array()).pow(i)).matrix();
}


