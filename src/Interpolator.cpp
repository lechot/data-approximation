
#include <iostream>
#include "Interpolator.h"

Interpolator::Interpolator(const Data &data) {

    X = data.getX();
    Y = data.getY();
    nb_samples = X.size();

    if(X.size() != Y.size()){
        std::cerr << "Unmatching dimensions for X and Y !" << std::endl;
    }
}


int Lagrange::index_of_node(double x){
    for (int s(0); s< nb_samples; s++){
        if (x==X(s)) { return s; }
    }
}


double Lagrange::prod_of_diff(double x, int idx) {
    double product=1;
    for (int i(0); i<nb_samples; ++i) {

        if (i != idx) { product *= (x - X(i)); }
    }
    return product;
}


Lagrange::Lagrange(const Data &data): Interpolator(data) {
    Eigen::VectorXd temp(nb_samples);
    for (int i(0); i<nb_samples; i++){
        temp(i)= prod_of_diff( X(i), i );

        //assert that the nodes are distinct
        if( temp(i) == 0 ){
            std::cerr << "Nodes should be distinct !" << std::endl;
        }
    }
    L=temp;
}

Data Lagrange::Standard(Eigen::VectorXd Z){
    int N= Z.size();
    Eigen::VectorXd temp(N);

    //loop over input points
    for (int i(0); i<N; i++) {

        //loop over initial nodes
        for (int j(0); j< nb_samples; j++){
            temp(i) += Y(j) * ( prod_of_diff( Z(i), j) / L(j));
        }
    }
    Data data(Z, temp);
    return data;
};

Data Lagrange::BaryCentric(Eigen::VectorXd Z){
    int N= Z.size();
    Eigen::VectorXd res(N);
    double numerator;
    double denominator;
    double temp;

    //loop over input points
    for (int i(0); i<N; i++) {
        numerator= 0.0;
        denominator= 0.0;
        temp=0.0;

        //loop over initial nodes
        for (int j(0); j< nb_samples; j++){

            //if the point is a node
            if (prod_of_diff( Z(i), j ) == 0){
                int idx= index_of_node(Z(i));
                numerator= Y(idx); denominator=1.0;
                break;
            }
            else {
                temp = 1 / (L(j) * (Z(i) - X(j)));
                numerator += temp * Y(j);
                denominator += temp;
            }
        }
        res(i)= numerator/denominator;
    }
    Data data(Z, res);
    return data;
}


std::complex<double> Periodic::ck(int k){
    std::complex<double> i(0,1);
    std::complex<double> c(0,0);
    double h= (2*M_PI)/nb_samples;


    for (int j(0); j<nb_samples; j++){
        c+= Y(j)* std::exp( -j*k*h*i );
    }
    return c/ ((double) nb_samples);
}

Periodic::Periodic(const Data &data): Interpolator(data) {
    int N= nb_samples-1;
    is_odd= N%2;
    M= is_odd? (N-1)/2: N/2;
    mew= N%2;
    Eigen::VectorXcd temp(M+1);

    for (int i(0); i<=M; i++){
        temp(i)= ck( i );
    }
    C=temp;
}

Data Periodic::Fourier(Eigen::VectorXd Z){
    int N= Z.size();
    Eigen::VectorXd res(N);
    std::complex<double> i(0,1);

    //loop over each input point
    for  (int j(0); j<N; j++){
        std::complex<double> temp= C(0);
        if (is_odd){
            std::complex<double> p= ck(M+1)/2.0;
            std::complex<double> q= ck(-M-1)/2.0;
            temp+= p*std::exp( (M+1)*Z(j)*i ) + q*std::exp( -(M+1)*Z(j)*i );
        }

        for (int k(1); k<=M; k++){
            temp+= C(k)*std::exp( k*Z(j)*i ) + conj(C(k)) *std::exp( -k*Z(j)*i );
        }
        res(j)= temp.real();
    }
    Data data(Z, res);
    return data;
}