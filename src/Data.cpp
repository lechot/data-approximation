//
// Created by lechot@INTRANET.EPFL.CH on 02.12.22.
//

#include <iostream>
#include <fstream>
#include "Data.h"

void Data::print() const {
    std::cout << "Points : " << std::endl;
    std::cout << X << std::endl;
    std::cout << "Values : " << std::endl;
    std::cout << Y << std::endl;
}

Eigen::VectorXd Data::getX() const {
    return X;
}

Eigen::VectorXd Data::getY() const {
    return Y;
}

void Data::write(std::ofstream& output_file) const {
    for(int i(0); i < X.size(); ++i){
        output_file << X[i] << " " << Y[i] << std::endl;
        assert(output_file.good());
    }
}

void Data::read(std::ifstream& input_file, int n_rows){
    X = Eigen::VectorXd::Zero(n_rows);
    Y = Eigen::VectorXd::Zero(n_rows);
    for(int i(0); i < n_rows; ++i){
        input_file >> X[i] >> Y[i];
    }
}

Data& Data::operator= (const Data& data) {
    X = data.X;
    Y = data.Y;
}

