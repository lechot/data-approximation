//
// Created by lechot@INTRANET.EPFL.CH on 02.12.22.
//

#ifndef PCSC_PROJECT_LEASTSQUARES_H
#define PCSC_PROJECT_LEASTSQUARES_H

#include <Eigen>
#include "Data.h"

/**
 * This class provides all the necessary tools to perform a least squares approximation of input data X and Y, and
 * to evaluate the resulting function at some specified query points X_eval.
 */

class LeastSquares {

public :

    /** \brief Constructor that initializes the attributes except "beta", that will be modified by the method "fit".
     *
     */
    LeastSquares(const Data& data, int nb_basis_elements);

    /** \brief Method that performs the fit and updates the "beta" attribute, that contains the fitted coefficients.
     *
     */
    void fit();
    /** \brief Method that returns the fitted coefficients "beta".
     *
     */
    Eigen::VectorXd get_beta() const {
        return beta;
    }
    /** \brief Method that evaluates the fitted function at the query points "X_eval" and returns the result as a Data object.
     *
     */
    Data evaluate(const Eigen::VectorXd& X_eval) const;
    /**
     * Methods that calls "fit" and "evaluate" and returns the result in a Data object.
     */
    Data run(const Eigen::VectorXd& X_eval);

protected :
    Eigen::VectorXd X;
    Eigen::VectorXd Y;
    int nb_basis_elements;
    int nb_samples;
    Eigen::MatrixXd design_matrix;
    Eigen::VectorXd beta;

    /** \brief Method that builds the design matrix, calling the function "basis_function".
     *
     */
    void build_design_matrix();
    /** \brief This virtual method will be overridden in the daughter classes, using the appropriate basis functions.
     *
     * @param i : index of the basis function
     * @param X : vector of query points
     * @return : vector of basis function evaluated at query points
     */
    virtual Eigen::VectorXd basis_function(int i, const Eigen::VectorXd& X) const = 0;

};

/**
 * This class inherits from the LeastSquares class, and overrides the virtual "basis_function" method, using
 * a standard polynomial basis.
 */

class PolynomialLeastSquares : public LeastSquares {

public:
    PolynomialLeastSquares(const Data& data, int nb_basis_elements);

protected:
    /** \brief This method overrides the virtual method "basis_function" and uses a standard polynomial basis.
     *
     */
    Eigen::VectorXd basis_function(int i, const Eigen::VectorXd& X) const override;

};


#endif //PCSC_PROJECT_LEASTSQUARES_H
