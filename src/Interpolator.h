//
// Created by lechot@INTRANET.EPFL.CH on 02.12.22.
//

#ifndef PCSC_PROJECT_INTERPOLATOR_H
#define PCSC_PROJECT_INTERPOLATOR_H

#include <complex>
#include <cmath>
#include <Eigen>
#include "Data.h"

/**
 * This class provides tools to interpolate input data X and Y, and
 * to extrapolate the resulting function at some specified query points X_eval.
 */


class Interpolator {

public:
    /** \brief Constructor that captures a Data type object and separates the dependent (Y) and
     * independent (X) variables to interpolate, recording the number of interpolation points (nodes) used.
     * @param data : Data type object containing the nodes X and their images Y
    */

    Interpolator(const Data& data);

protected:
    Eigen::VectorXd X;
    Eigen::VectorXd Y;
    int nb_samples;
};

/**
 * This class inherits from Interpolator class and offers two Lagrange interpolation methods.
 */

class Lagrange : public Interpolator {

public:
    /** \brief Constructor that utilizes the mother constructor and calls "prod_of_diff" method to set an attribute L that will be used by the
     * Standard and Barycentric methods
    */

    Lagrange(const Data &data);

    /** \brief A method that interpolates the input data using Standard Lagrange Interpolation and outputs
     * the evaluation of the query points using the resulting interpolant.
     *
     * @param Z : vector of query points
     * @return : Data object containing the query points Z with their corresponding Standard Lagrange extrapolation
     */
    Data Standard(Eigen::VectorXd Z);

    /** \brief A variant of Lagrange Interpolation method that interpolates the input data using Bary Centric Interpolation and outputs
     * the evaluation of the query points using the resulting interpolant.
     *
     * @param Z : vector of query points
     * @return : Data object containing the query points Z with their corresponding Bary Centric extrapolation
     */
    Data BaryCentric(Eigen::VectorXd Z);

protected:

    /** \brief Method that computes the product of difference of the input x and the initial nodes with index
     * different that idx
     * @param x : double scalar
     * @param ind : index of node
     * @return : Product of (x-X(i)) for i!=idx, where X is the vector of nodes
     */
    double prod_of_diff(double x, int idx);

    /** \brief Method that computes the index of the node whose value equals the input x
     * @param x : scalar
     * @return : Integer i such that X(i)=x
     */
    int index_of_node(double x);
    Eigen::VectorXd L;
};

/**
 * This class inherits from the Interpolator class and offers Fourier (Trigonometric) Interpolation for 2PI-periodic
 * functions
 */
class Periodic : public Interpolator {

public:

    /** \brief Constructor that accepts Data with nodes in one period and sets the fourier coefficients using "ck" method
     * @param data : Data object with a specific form for the nodes: data.X(i)= i*(2PI/nb_samples) for i=0,..., nb_samples-1
     */
    Periodic(const Data &data);

    /** \brief A method that interpolates periodic input data using Fourier Interpolation and outputs
     * the evaluation of the query points using the resulting interpolant.
     *
     * @param Z : vector of query points
     * @return : Data object containing the query points Z with their corresponding Fourier extrapolation
     */
    Data Fourier(Eigen::VectorXd Z);

protected:

    /** \brief Method that computes the k_th fourier coefficient
  * @param k : index of the coefficient to be calculated
  * @return  : complex fourier coefficient of index k
  */
    std::complex<double> ck(int k);
    bool is_odd;
    int M;
    int mew;
    Eigen::VectorXcd C;
};

#endif //PCSC_PROJECT_INTERPOLATOR_H
