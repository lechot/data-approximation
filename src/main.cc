#include <iostream>
#include <Eigen>
#include "Data.h"
#include "LeastSquares.h"
#include "Interpolator.h"
#include <fstream>

Data run_least_squares(const Data& input_data, int nb_basis_elements, const Eigen::VectorXd& X_eval);
Data run_interpolator(const Data& input_data, std::string interpolation_method, const Eigen::VectorXd& X_eval);
void handle_input(int argc, char* argv[], std::string& program_name, std::string& operation, std::string& input_file_name, int& nb_rows, std::string& interpolation_method, int& nb_basis_elements, double& x_min, double& x_max, int& n_points);

int main(int argc, char* argv[]) {

    // INPUT

    std::string program_name;
    std::string operation;
    std::string input_file_name;
    int nb_rows;
    std::string interpolation_method;
    int nb_basis_elements;
    double x_min;
    double x_max;
    int n_points;

    // update the above parameters with the user input, checking the validity
    handle_input(argc, argv, program_name, operation, input_file_name, nb_rows, interpolation_method, nb_basis_elements, x_min, x_max, n_points);

    // print to console a summary of the parameters
    std::cout << "Input file : " << input_file_name << ", with " << nb_rows << " rows." << std::endl;
    std::cout << "Operation : " << operation << std::endl;
    if(operation == "interpolate") {
        std::cout << "Interpolation method : " << interpolation_method << std::endl;
    } else {
        std::cout << "Number of basis elements : " << nb_basis_elements << std::endl;
    }
    std::cout << n_points << " evaluation points between " << x_min << " and " << x_max << std::endl;

    // SET-UP

    // builds a data object reading the input file
    std::ifstream input_file("../input_files/" + input_file_name);
    if(not input_file.is_open()){
        std::cerr << "Input file " << "../input_files/" + input_file_name << " not found. Please paste it in the cmake-build-debug folder." << std::endl;
        return 0;
    }
    Data data(input_file, nb_rows);
    input_file.close();
    auto X_eval = Eigen::VectorXd::LinSpaced(n_points,x_min,x_max);

    // FITTING OR INTERPOLATING
    // depending on the input, call least-squares or interpolator
    Data data_eval;
    if(operation == "fit"){
        data_eval = run_least_squares(data, nb_basis_elements, X_eval);
    } else {
        data_eval = run_interpolator(data, interpolation_method, X_eval);
    }

    // WRITING OUTPUT

    std::string output_file("../output/output.dat");
    std::ofstream output_stream(output_file);
    assert(output_stream.is_open());
    std::cout << "Writing to " << output_file << " ..." << std::endl;
    data_eval.write(output_stream);
    output_stream.close();

    std::cout << "Finished writing." << std::endl;

    return 0;
}

void handle_input(int argc, char* argv[], std::string& program_name, std::string& operation, std::string& input_file_name, int& nb_rows, std::string& interpolation_method, int& nb_basis_elements, double& x_min, double& x_max, int& n_points){
    program_name = argv[0];
    operation = argv[1];
    while (not (operation == "interpolate" or operation == "fit")){
        std::cout << "Invalid operation, choose between interpolate or fit : " << std::endl;
        std::cin >> operation;
    }
    input_file_name = argv[2];
    nb_rows = atoi(argv[3]);
    while(nb_rows <= 0){
        std::cout << "Number of rows in the input file needs to be a positive integer : " << std::endl;
        std::cin >> nb_rows;
    }
    if(operation == "interpolate") {
        interpolation_method = argv[4];
        while(not ((interpolation_method == "standard") or (interpolation_method == "barycentric") or (interpolation_method == "fourier"))){
            std::cout << "Invalid interpolation method, choose between standard, barycentric or fourier : " << std::endl;
            std::cin >> interpolation_method;
        }
    } else if (operation == "fit") {
        nb_basis_elements = atoi(argv[4]);
        while(nb_basis_elements <= 0){
            std::cout << "Number of basis elements for least squares fitting needs to be a positive integer : " << std::endl;
            std::cin >> nb_basis_elements;
        }
    }
    x_min = atof(argv[5]);
    x_max = atof(argv[6]);
    while(x_max <= x_min){
        std::cout << "Xmax needs to be strictly bigger than " << x_min << " :" << std::endl;
        std::cin >> x_max;
    }
    n_points = atoi(argv[7]);
}

Data run_interpolator(const Data& input_data, std::string interpolation_method, const Eigen::VectorXd& X_eval){
    Data output_data;
    if (interpolation_method=="standard"){
        Lagrange L1(input_data);
        output_data= L1.Standard(X_eval);
    } else if (interpolation_method=="barycentric"){
        Lagrange L2(input_data);
        output_data= L2.BaryCentric(X_eval);
    } else {
        Periodic P1(input_data);
        output_data= P1.Fourier(X_eval);
    }
    return output_data;
}

Data run_least_squares(const Data& input_data, int nb_basis_elements, const Eigen::VectorXd& X_eval){
    PolynomialLeastSquares poly_ls(input_data, nb_basis_elements);
    auto data_eval(poly_ls.run(X_eval));
    std::cout << "Least squares coefficients : " << std::endl << poly_ls.get_beta() << std::endl;
    return data_eval;
}