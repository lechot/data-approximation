//
// Created by lechot@INTRANET.EPFL.CH on 02.12.22.
//

#ifndef PCSC_PROJECT_DATA_H
#define PCSC_PROJECT_DATA_H

#include <Eigen>

/** This class provides the tools to read input data from file and write output data to file. It also serves
 * as a convenient argument and return type for interpolating and fitting methods.
 */

class Data {

private :
    Eigen::VectorXd X;
    Eigen::VectorXd Y;

public :
    Data() = default;

    /** \brief This constructor reads input data from file and stores it in the attributes X and Y. Input file needs
     * to have one column for X and one column for Y.
     *
     * @param input_file : the input stream, having one column for X and one column for Y
     * @param n_rows : the number of rows in the input file
     */
    Data(std::ifstream& input_file, int n_rows) {
        this->read(input_file, n_rows);
    }

    /** \brief This constructor takes as input a vector X and a vector Y of the same size, and stores them in the
     * attributes X and Y.
     *
     * @param X : Independant values.
     * @param Y : Dependant values.
     */
    Data(Eigen::VectorXd X, Eigen::VectorXd Y)
    : X(X), Y(Y)
    {}

    /** \brief This method returns the X attribute.
     */
    Eigen::VectorXd getX() const;
    /** \brief This method returns the Y attribute.
     */
    Eigen::VectorXd getY() const;

    /** \brief This method prints the attributes X and Y to the console.
     *
     */
    void print() const;

    // read and write
    /** \brief This method reads the first "n_rows" rows of the input file, that has one column for X and one column for Y.
     *
     * @param input_file
     * @param n_rows
     */
    void read(std::ifstream& input_file, int n_rows);
    /** \brief This method writes the attributes X and Y to an output file, in column format.
     * @param output_file
     */
    void write(std::ofstream& output_file) const;

    /** \brief This operator copies all the attributes of the argument data into the current Data object.
     */
    Data& operator= (const Data& data);

};


#endif //PCSC_PROJECT_DATA_H
