**DESCRIPTION**

This program can be used to find the least-squares approximation of response data Y from explanatory data X using
an arbitrary degree polynomial basis, and to interpolate data using either the standard
Lagrange, the barycentric Lagrange, or the Fourier method.

**COMPILATION**

In order to compile the program, one should first install *googletest* :

```
git submodule update --init 
```

Then, building is done as usual, e.g. with CLion or in the terminal:

```
mkdir build
cd build
cmake ..
make
```

**USAGE**

*Input data:*
The input data should be written in a *.dat* or *.txt* file, using one column
for the X data and one column for the Y data (ie, such that $\`Y_i = f(X_i)`$),
*f* being the underlying unknown function that produced the data.

*Execution:*
Once the project is built, the user needs to execute the target *main* from
the command line (*main* should be in a /build folder). In case the user wants
to interpolate the data, he needs to enter the following
parameters :

```
./main interpolate input_file nb_rows interpolation_method x_min x_max nb_points
```

- *interpolate* specifies that the user wants to perform an interpolation
- *input_file* is the name of the input file located in */input_files*. It must
contain a first column for X data and a second column for Y data. Two
examples are provided here : *input_test_lag.txt* and *input_test_fourier.txt*, 
which can serve as examples for a typical execution of the program.
*input_test_lag.txt* should be used with Lagrange Standard or Barycentric methods,
while *input_test_fourier.txt* can be used with Fourier method (data is between
0 and 2PI).
- *nb_rows* specifies the number of samples in the input file
- *interpolation_method* can be *standard* for Standard Lagrange, *barycentric*
for Barycentric Lagrange, or *fourier* for Fourier interpolation.
- *x_min* and *x_max* are the boundaries of the interval in which the
result of the interpolation will be evaluated (results can be found
in output/output.dat)
- *nb_points* specifies the number of points in this interval

Here are the commands to run the provided examples :

```
./main interpolate input_test_lag.txt 41 standard -20 20 101
```
or
```
./main interpolate input_test_lag.txt 41 barycentric -20 20 101
```
or
```
./main interpolate input_test_fourier.txt 20 fourier 0 6 101
```

To fit the data using the least squares method, one needs
to type the following command in the command line :

```
./main fit input_file nb_rows nb_basis_elements x_min x_max nb_points
```

- *fit* specifies that the user wants to fit the data
- *input_file* is the name of the input file located in */input_files*. It must
  contain a first column for X data and a second column for Y data. One
  example is provided here : *least_squares_case.txt*, which can serve as
an example for a typical execution of the program.
- *nb_rows* specifies the number of rows of the input file
- *nb_basis_elements* specifies the number of basis elements the user
wants to use for the fit. In the case of a polynomial fitting, it 
corresponds to *degree + 1*.
- *x_min* and *x_max* are the boundaries of the interval in which the
  result of the interpolation will be evaluated (results can be found
  in output/output.dat)
- *nb_points* specifies the number of points in this interval

Here are the commands to run the provided example :

```
./main fit least_squares_case.txt 101 6 -3 1 101
```

**Flow**
During the execution, the user input is handled by a function *handle_input*,
which prepares the input to be given to the fitting or interpolating methods.
It uses in particular the Data class, which allows reading from file the explanatory data X and the dependant data Y.

For *interpolate*, if *standard* and *barycentric* was chosen, the constructor instantiates an Eigen::VectorXd which will be shared by both methods, 
using the *prod_of_diff*. This method computes the difference of an input scalar with each initial node, 
and output their corresponding product. Afterwards, a Data object is created containing the user evaluation input as X and their extrapolations as Y
using the standard lagrange formula or the barycentric formula. 
Finally, the *write* method of the Data class is used to write the evaluated X and Y to an output file. Similarly, calling *fourier* 
will instantiate an Eigen::VectorXd containing the fourier coefficients used to evaluate the user evaluation points at the 
interpolant using fourier interpolating formula.

If *fit* was chosen, the method *run_least_squares* is called. It will
instantiate a PolynomialLeastSquares object (if more fitting methods are
implemented, one should introduce a condition to instantiate a different
object). In its constructor, this
PolynomialLeastSquares object uses its own *basis_function* method to
build the design matrix A. Then, the method *run* of
the PolynomialLeastSquares object is called. This method computes the fitting
parameters and prints them to screen, and then evaluates the fitted
function at the user specified evaluation points. It returns a Data object
that contains the evaluation points and the evaluated values. Finally,
the *write* method of the Data class is used to write the evaluated X
and Y to an output file.
Note : In order to compute the least-squares solution of $A\beta = Y$, 
the Eigen library provides several tools that differ in speed and stability.
A middle-speed but quite stable method was chosen here, namely the
*colPivHouseholderQr()* method, which uses the QR-decomposition and
pivoting to deal with singular matrices. Even if the design matrix A
should not be singular in our case, we chose this option for robustness.
For more information, see https://eigen.tuxfamily.org/dox/group__LeastSquares.html

**FEATURES**

A Data class provides the tools to read input data from file as well
as write data to output file. It also serves as a convenient feature
to give to the LeastSquares/Interpolator objects as an argument or a return type.

The LeastSquares class provides the tools to set-up the least-squares
fitting problem and to perform it. It is an abstract class because
it has a purely virtual method *basis_function*, that defines the
basis functions that can be used to express the fitted function. A single
daughter class, PolynomialLeastSquares, inherits from LeastSquares.
It can perform least-squares fitting for an arbitrary polynomial degree.
In order to extend to other types of basis functions (such as trigonometric),
one should essentially just override the *basis_function* method of the
parent class LeastSquares. However, if one wants to fit more involved 
laws (such as power law), one would have to do a bit of work on the input
to put the problem in a linear algebraic form. 
This preprocessing could be done in the constructor of the LeastSquares
class (e.g. taking *logs* of the data for the power law to become a
linear combination).

The Interpolator class provides the tools to set-up an interpolation problem and execute it. It has only a constructor that sets up a Data 
object to be used by daughter classes. One daughter class is *Lagrange* which offers two interpolation methods *standard* and *barycentric*. 
These methods are best suited for polynomials of any degree, but are capable of interpolating any given function and evaluate it at different 
input points.
Another class which inherits from *Interpolator* is *Periodic*. This class is suited for 2PI-periodic functions. It uses *fourier* method to interpolate 
the periodic data and also extrapolate at given points. 





**TESTS**

Tests have been written for the classes *Data*, *LeastSquares*, *Interpolator*
and their subclasses using the *Googletest* framework. They are 
compiled in the same way as the *main* program (see COMPILATION section
on the top). The executables are called *tests_data* (for Data class),
*tests* (for LeastSquares class) and *tests_interpolator* (for Interpolator
class). They are simply executed by :

```
./tests_data
```
or
```
./tests
```
or
```
./tests_interpolator
```
For the Data class, there are tests for the constructor taking 
*Eigen::VectorXd* vectors as argument, for the constructor taking an input file
as argument, and for the overridden = operator.
For the LeastSquares class, the methods *fit* and *evaluate*
have been tested using a *PolynomialLeastSquares* instance. The test case
(in file *input_test_ls.txt*) is taken from Matlab, and compared 
to the Matlab result obtained by the curve fitting tool.
For the Interpolator class, the methods *standard*, *barycentric*, and *fourier*
have been tested using a *Lagrange* instance and a test case
(in file *input_test_lag.txt*) for the first two, and a *Periodic* instance
and a test case (in file *input_test_fourier.txt*)for the third. The test cases
were generated from Eigen functionalities, with interpolation points generated from 
a square function for the former case, and a sine function for the latter.


**ISSUES AND PERSPECTIVES**

First, minor changes could be done to make the input more convenient.
For example, the *read* method from the Data class could probably avoid
taking the number of rows of the input file as an argument. Also, the
main.cc script could be made more compact using a more extensive *handle_input*
function.
In order to clearly separate the solving part (interpolation or fitting)
from the post-processing part (evaluation of the obtained function,
plot, etc), one could add a class *Function* that handles this last part.
Currently, the evaluation part is directly handled by the classes that
perform the solving part. Adding this *Function* class would also
allow developing the post-processing part (add plotting for instance).
As a last comment, we noticed that we missed an opportunity to factorize the code more.
Indeed, fourier interpolation can be reformulated such that it only differs from the lagrangian method by the replacement of the lagrange polynomials by a polynomial formulation in the complex plane. Thus, if more time was allowed, we would have done a virtual method in the
parent class *Interpolator*, and define the basis function in the daughter classes.
Finally, as a more robust statistical approach, one could provide the uncertainties
on the fitted coefficients, the coefficient of determination, etc.
